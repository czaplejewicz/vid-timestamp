# !/usr/bin/python3
from PIL import Image, ImageDraw, ImageFont
from pathlib import Path
from tempfile import TemporaryDirectory
from multiprocessing import Pool
from datetime import datetime
import subprocess
from contextlib import contextmanager
from io import BytesIO

font = ImageFont.truetype("LiberationMono-Regular.ttf", 40)

def get_path_date(img_path):
    return datetime.strptime(img_path.stem, "%Y%m%d-%H%M%S-%f")

def get_exp_name(path):
    return path.name.split('-')[0]

def stamp_time(img_path, date, first_date):
    img = Image.open(img_path.as_posix())
    draw = ImageDraw.Draw(img)
    draw.text((0, 0), datetime.strftime(date, "%H:%M:%S.%f"), font=font)
    draw.text((0, 40), "{:>10.3f}".format((date - first_date).total_seconds()), font=font)
    return img

def stamp_date(img, date):
    draw = ImageDraw.Draw(img)
    draw.text((0, 80), date.isoformat(), font=font)

def process_img(arg):
    (i, (date, path)), first_date, temppath = arg
    do_img(i, date, path, first_date, temppath)


def do_img(i, date, path, first_date, temppath=None, pipe=None):
    print(i, path)
    image = stamp_time(path, date, first_date)
    if i == 0:
        stamp_date(image, date)
    if pipe is None:
        image.save(temppath.joinpath("{:08d}.tiff".format(i)), "tiff")
    else:
        buf = BytesIO()
        image.save(buf, 'bmp')
        pipe.write(buf.getvalue())
        

def path_images(path):
    return (p for p in path.iterdir() if p.suffix == '.tiff')

def stamp_dates(path, temppath):
    dated_paths = [(get_path_date(img_path), img_path) for img_path in path_images(path)]
    enumerated_paths = enumerate(sorted(dated_paths))
    first_date = sorted(dated_paths)[0][0]
    with Pool(2) as p:
        stamped = p.map(process_img, ((pa, first_date, temppath) for pa in enumerated_paths))


def make_video(name, imgpath):
    subprocess.run(['ffmpeg',
                    '-start_number', '00000000',
                    '-i', '{}/%08d.tiff'.format(imgpath),
                    '-vcodec', 'libx264',
                    '{}.mkv'.format(name)],
                   check=True)

def make_dated_video(path, tmp):
    exp_name = get_exp_name(path)
    with TemporaryDirectory(dir=tmp.as_posix()) as tmpdir:
        tmppath = Path(tmpdir)
        stamp_dates(path, tmppath)
        make_video(exp_name, tmppath)

def stream_stamps(path, codec):
    dated_paths = [(get_path_date(img_path), img_path) for img_path in path_images(path)]
    enumerated_paths = enumerate(sorted(dated_paths))
    first_date = sorted(dated_paths)[0][0]
    for i, (date, path) in enumerated_paths:
        do_img(i, date, path, first_date, pipe=codec.stdin)

@contextmanager
def Image2Pipe(name):
    p = subprocess.Popen(['ffmpeg', '-y',
                          '-s', '1024x768',
                          '-pix_fmt', 'rgb24',
                          '-f', 'image2pipe',
                          '-vcodec', 'bmp',
                          '-i', '-',
                          '-vcodec', 'libx264',
                          '{}.mkv'.format(name)], stdin=subprocess.PIPE)
    try:
        yield p
    finally:
        p.stdin.close()
        p.wait()

def make_streamed_video(path):
    exp_name = get_exp_name(path)
    with Image2Pipe(exp_name) as codec:
        stream_stamps(path, codec)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("directory", help="path to directory containing pics. Name must be: expname-date-hour-ms")
    parser.add_argument('--tmp')
    args = parser.parse_args()
    if args.tmp:
        make_dated_video(Path(args.directory), Path(args.tmp))
    else:
        make_streamed_video(Path(args.directory))
