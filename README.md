Video timestamper
=================

This program takes images produced with coriander (`png`s with timestamps in file name) and turns them into a video file with timestamps overlaid.

Requirements
------------

- Python3
- Pillow (a.k.a. PIL)
- ffmpeg
- libx264

